package com.afs.todo.controller;

import com.afs.todo.pojo.Todo;
import com.afs.todo.repository.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {

    @BeforeEach
    void setUp() {
        todoRepository.deleteAll();
    }

    @Autowired
    private MockMvc client;

    @Autowired
    private TodoRepository todoRepository;

    @Test
    void should_return_todos_when_perform_get_all_todos_given_todos_in_db() throws Exception {
        Todo todo1 = new Todo("do something1", false);
        Todo todo2 = new Todo("do something2", false);

        todoRepository.saveAll(List.of(todo1, todo2));
        client.perform(MockMvcRequestBuilders.get("/todos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].text").value(todo1.getText()))
                .andExpect(jsonPath("$[0].done").value(todo1.getDone()))
                .andExpect(jsonPath("$[1].text").value(todo2.getText()))
                .andExpect(jsonPath("$[1].done").value(todo2.getDone()));
    }

    @Test
    void should_return_todo_when_perform_post_given_text() throws Exception {
        String newTodo = "{" +
                "\"text\":\"do something2\"," +
                "\"done\":false}";

        client.perform(MockMvcRequestBuilders.post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newTodo))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("do something2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));
    }

    @Test
    void should_return_todo_when_perform_get_todo_by_id_given_id() throws Exception {
        Todo todo1 = new Todo("do something1", false);

        todoRepository.save(todo1);
        client.perform(MockMvcRequestBuilders.get("/todos/{id}",todo1.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value(todo1.getText()))
                .andExpect(jsonPath("$.done").value(todo1.getDone()));
    }

    @Test
    void should_return_todo_with_response_when_perform_put_todo_given_id_and_text() throws Exception {
        Todo todo1 = new Todo("do something1", false);
        todoRepository.save(todo1);

        String todoToUpdate = "{" +
                "\"text\":\"update something\"," +
                "\"done\":false}";

        client.perform(MockMvcRequestBuilders.put("/todos/{id}",todo1.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoToUpdate))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("update something"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));
    }

    @Test
    void should_return_204_when_perform_delete_given_id() throws Exception {
        Todo todo1 = new Todo("do something1", false);
        todoRepository.save(todo1);

        client.perform(MockMvcRequestBuilders.delete("/todos/{id}",todo1.getId()))
                .andExpect(status().isNoContent());
    }

    @Test
    void should_return_todo_with_reverse_done_status_perform_put_todo_given_id() throws Exception {
        Todo todo1 = new Todo("do something1", false);
        todoRepository.save(todo1);

        String todoToUpdate = "{" +
                "\"text\":\"update something\"," +
                "\"done\":true}";

        client.perform(MockMvcRequestBuilders.put("/todos/{id}",todo1.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoToUpdate))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("update something"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(true));
    }

    @Test
    void should_return_404_when_perform_delete_by_id_given_id_not_exist() throws Exception {
        // given

        // when
        client.perform(MockMvcRequestBuilders.delete("/todos/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // should
    }
}
