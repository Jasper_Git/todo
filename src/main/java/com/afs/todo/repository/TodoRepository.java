package com.afs.todo.repository;

import com.afs.todo.pojo.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TodoRepository extends JpaRepository<Todo,Integer> {
    List<Todo> findAllByDoneTrue();
}
