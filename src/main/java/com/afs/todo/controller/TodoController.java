package com.afs.todo.controller;

import com.afs.todo.dto.TodoCreateRequest;
import com.afs.todo.dto.TodoResponse;
import com.afs.todo.dto.TodoUpdateRequest;
import com.afs.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {

    @Autowired
    private TodoService todoService;

    @GetMapping
    public List<TodoResponse> getAllTodos() {
        return todoService.getAllTodos();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse createTodo(@RequestBody TodoCreateRequest todoCreateRequest) {
        return todoService.createTodo(todoCreateRequest);
    }

    @PutMapping("/{id}")
    public TodoResponse updateTodo(@PathVariable Integer id, @RequestBody TodoUpdateRequest todoUpdateRequest) {
        return todoService.updateTodo(id, todoUpdateRequest);
    }

    @GetMapping("/{id}")
    public TodoResponse getTodo(@PathVariable Integer id) {
        return todoService.getTodoWithResponse(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable Integer id) {
        todoService.deleteTodo(id);
    }
}
