package com.afs.todo.service;

import com.afs.todo.dto.TodoCreateRequest;
import com.afs.todo.dto.TodoResponse;
import com.afs.todo.dto.TodoUpdateRequest;
import com.afs.todo.exception.TodoNotFoundException;
import com.afs.todo.mapper.TodoMapper;
import com.afs.todo.pojo.Todo;
import com.afs.todo.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TodoService {
    @Autowired
    private TodoRepository todoRepository;

    public List<TodoResponse> getAllTodos() {
        return todoRepository.findAll().stream().map(TodoMapper::toResponse).collect(Collectors.toList());
    }


    public TodoResponse createTodo(TodoCreateRequest todoCreateRequest) {
        Todo todo = TodoMapper.toEntity(todoCreateRequest);
        todo.setDone(false);
        Todo todoSaved = todoRepository.save(todo);
        return TodoMapper.toResponse(todoSaved);
    }

    public TodoResponse getTodoWithResponse(Integer id) {
        return TodoMapper.toResponse(getById(id));
    }

    public Todo getById(Integer id) {
        Optional<Todo> todoFound = todoRepository.findById(id);
        if (todoFound.isPresent()) {
            return todoFound.get();
        }
        throw new TodoNotFoundException();
    }

    public TodoResponse updateTodo(Integer id, TodoUpdateRequest todoUpdateRequest) {
        Todo todoFound = getById(id);
        if (todoUpdateRequest.getText() != null) {
            todoFound.setText(todoUpdateRequest.getText());
        }
        if (todoUpdateRequest.getDone() != null) {
            todoFound.setDone(todoUpdateRequest.getDone());
        }
        todoRepository.save(todoFound);
        return TodoMapper.toResponse(todoFound);
    }

    public void deleteTodo(Integer id) {
        Todo todoFound = getById(id);
        todoRepository.deleteById(todoFound.getId());
    }

    public TodoResponse updateStatus(Integer id) {
        Todo todoFound = getById(id);
        todoFound.setDone(!todoFound.getDone());
        todoRepository.save(todoFound);
        return TodoMapper.toResponse(todoFound);
    }

    public List<TodoResponse> getTodosWithDone(Boolean done) {
        return todoRepository.findAllByDoneTrue().stream().map(TodoMapper::toResponse).collect(Collectors.toList());
    }
}
