package com.afs.todo.mapper;

import com.afs.todo.dto.TodoCreateRequest;
import com.afs.todo.dto.TodoResponse;
import com.afs.todo.pojo.Todo;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public static TodoResponse toResponse(Todo todo){
        TodoResponse todoResponse= new TodoResponse();
        BeanUtils.copyProperties(todo,todoResponse);
        return todoResponse;
    }

    public static Todo toEntity(TodoCreateRequest todoCreateRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoCreateRequest,todo);
        return todo;
    }
}
