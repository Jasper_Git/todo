package com.afs.todo.dto;

public class TodoCreateRequest {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
