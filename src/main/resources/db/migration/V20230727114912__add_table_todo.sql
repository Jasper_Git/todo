drop table if exists `todo`;
CREATE TABLE `todo` (

`id` int NOT NULL AUTO_INCREMENT,

`text` varchar(255) NOT NULL,

`done` tinyint NOT NULL DEFAULT 0,

PRIMARY KEY (`id`)

);